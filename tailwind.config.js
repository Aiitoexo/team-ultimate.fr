module.exports = {
  purge: [
      './resources/**/*.blade.php',
      './resources/**/*.js',
      './resources/**/*.vue',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
        colors: {
            'grey': {
                DEFAULT: '#171718',
                '50': '#88888D',
                '100': '#7B7B80',
                '200': '#626266',
                '300': '#49494C',
                '400': '#303032',
                '500': '#171718',
            },
        }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
